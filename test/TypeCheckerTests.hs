{-# LANGUAGE OverloadedStrings #-}

module TypeCheckerTests where

import Data.Bifunctor
import Testing
import Language.CP.TypeChecker

typeCheckerTests :: TestTree
typeCheckerTests = testGroup "TypeChecker tests"
  [ testCase "TypeChecker 01" $ assertEq t01
  , testCase "TypeChecker 02" $ assertEq t02
  , testCase "TypeChecker 03" $ assertEq t03
  , testCase "TypeChecker 04" $ assertEq t04
  , testCase "TypeChecker 05" $ assertEq t05
  , testCase "TypeChecker 06" $ assertEq t06
  , testCase "TypeChecker 07" $ assertEq t07
  , testCase "TypeChecker 08" $ assertEq t08
  , testCase "TypeChecker 09" $ assertEq t09
  ]

type TypeCheckTest1 =
  ((Either TypeErr Type, State), (Either TypeErr Type, State))

type TypeCheckTest2 =
  (Either (Either String TypeErr) (), Either (Either String TypeErr) ())

t01 :: TypeCheckTest1
t01 =
  ( runInferE
      (App "blah" [Value $ VBool True, Value $ VInt 5])
      initState
        { sEnv =
           [[EVar ("blah", TFun [TCon "Bool", TCon "Int"] (TCon "Void"))]]
        }
          
  , ( Right (TCon "Void")
    , initState
      { sEnv =
         [[EVar ("blah", TFun [TCon "Bool", TCon "Int"] (TCon "Void"))]]
      }
    )
  )



t02 :: TypeCheckTest1
t02 =
  ( runInferE
      (App "boo" [App "blah" [Value $ VBool True, Value $ VInt 5], Var "baba"])
      initState
        { sEnv =
          [[EVar ("boo", TFun [TCon "Int", TVar "t0"] (TVar "t0"))
           ,EVar ("baba", TVar "t0")
           ,EVar ("blah", TFun [TCon "Bool", TCon "Int"] (TCon "Int"))
          ]]
        }

  , ( Right (TVar "t0")
    , initState
      { sEnv =
          [[EVar ("boo", TFun [TCon "Int", TVar "t0"] (TVar "t0"))
           ,EVar ("baba", TVar "t0")
           ,EVar ("blah", TFun [TCon "Bool", TCon "Int"] (TCon "Int"))
          ]]
      }
    )
  )


parseAndTCheck :: String -> Either (Either String TypeErr) ()
parseAndTCheck prog = do
  r <- first Left $ parse "test" prog
  first Right $ checkProgram r

t03 :: TypeCheckTest2
t03 =
  ( parseAndTCheck $ unlines
    [ "fun hello[] -> Int {"
    , "  5; 6; 7;"
    , "  val x : Int = 1;"
    , "  x;"
    , "};"
    , ""
    , "fun hi[x : Int] -> Int {"
    , "  x;"
    , "};"
    , "val y : Bool = false;"
    , ""
    , "val z : Int = 1;"
    ]

  , Right ()
  )


t04 :: TypeCheckTest2
t04 =
  ( parseAndTCheck $ unlines
    [ "fun hello[] -> Int {"
    , "  y;"
    , "};"
    , ""
    , "val y : Bool = false;"
    ]

  , Left (Right (TypeMismatch (Just (Var "y")) (TCon "Int") (TCon "Bool")))
  )

t05 :: TypeCheckTest2
t05 =
  ( parseAndTCheck $ unlines
    [ "fun id[y : Int] -> Int {"
    , "  y;"
    , "};"
    , ""
    , "val y : Bool = false;"
    ]

  , Right ()
  )

t06 :: TypeCheckTest2
t06 =
  ( parseAndTCheck $ unlines
    [ "fun id[y : Int] -> Int {"
    , "  {"
    , "    val y : Bool = false;"
    , "    5;"
    , "  }"
    , "  y;"
    , "};"
    ]

  , Right ()
  )

t07 :: TypeCheckTest2
t07 =
  ( parseAndTCheck $ unlines
    [ "fun add2[y : Int] -> Int {"
    , "  add(y, 2);"
    , "};"
    ]

  , Right ()
  )

t08 :: TypeCheckTest2
t08 =
  ( parseAndTCheck $ unlines
    [ "fun main[] -> Void {"
    , "  val x : Int = 2;"
    , "  printInt(add2(x));"
    , "};"
    , "fun add2[y : Int] -> Int {"
    , "  add(y, 2);"
    , "};"
    ]

  , Right ()
  )

t09 :: TypeCheckTest2
t09 =
  ( parseAndTCheck $ unlines
    [ "fun main[] -> Void {"
    , "  printInt(doWith2(add,2));"
    , "};"
    , "fun doWith2[do : [Int, Int] -> Int, y : Int] -> Int {"
    , "  do(y, 2);"
    , "};"
    ]

  , Right ()
  )
