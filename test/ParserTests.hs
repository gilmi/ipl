{-# LANGUAGE OverloadedStrings #-}

module ParserTests where

import Testing

parserTests :: TestTree
parserTests = testGroup "Parser tests"
  [ testCase "Parse Function 01" $ assertEq pf01
  , testCase "Parse Function 02" $ assertEq pf02
  , testCase "Parse Function 03" $ assertEq pf03
  , testCase "Parse Program  01" $ assertEq pp01
  ]

type ParserTest =
  (Either String Program, Either String Program)

pf01 :: ParserTest
pf01 =
  (parse "pf01" $ unlines
    [ "fun hello[] -> Int {"
    , "  5; 6; 7;"
    , "  val x : Int = 1;"
    , "  x;"
    , "};"
    ]

  ,pure
     [DFun "hello" [] tint
       [SExpr (vint 5), SExpr (vint 6) 
       ,SExpr (vint 7) 
       ,SDecl (DVal "x" tint (vint 1))
       ,SExpr (Var "x")
       ]
     ]
  )


pf02 :: ParserTest
pf02 =
  (parse "pf02" $ unlines
    [ "fun block[x : Int] -> Int {"
    , "  {"
    , "    val x : Int = 1;"
    , "    x;"
    , "  }"
    , "  x;"
    , "};"
    ]

  ,pure
     [DFun "block" [("x", tint)] tint
       [SBlock
         [ SDecl (DVal "x" tint (vint 1))
         , SExpr (Var "x")
         ]
       ,SExpr (Var "x")
       ]
     ]
  )

pf03 :: ParserTest
pf03 =
  (parse "pf03" $ unlines
    [ "fun f[x : Int] -> [Int] -> Int {"
    , "  f;"
    , "};"
    ]

  ,pure
     [DFun "f" [("x", tint)] (TFun [tint] tint)
       [SExpr (Var "f")
       ]
     ]
  )


pp01 :: ParserTest
pp01 =
  (parse "pp01" $ unlines
    [ "fun hello[] -> Int {"
    , "  5; 6; 7;"
    , "  val x : Int = 1;"
    , "  x;"
    , "};"
    , ""
    , "fun hi[x : Int] -> Int {"
    , "  x;"
    , "};"
    , "val y : Bool = false;"
    , ""
    , "val z : Int = 1;"
    ]

  ,pure
     [DFun "hello" [] tint
       [SExpr (vint 5), SExpr (vint 6)
       ,SExpr (Value (VInt 7))
       ,SDecl (DVal "x" tint (vint 1))
       ,SExpr (Var "x")
       ]
     ,DFun "hi" [("x", tint)] tint
        [SExpr (Var "x")
        ]
     ,DVal "y" tbool (vbool False)
     ,DVal "z" tint (vint 1)
     ]
  )
