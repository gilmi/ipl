import Testing
import ParserTests
import TypeCheckerTests
import InterpreterTests

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests =
  testGroup
    "Tests"
    [ parserTests
    , typeCheckerTests
    , interpreterTests
    ]
