{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# LANGUAGE OverloadedStrings #-}

module InterpreterTests (interpreterTests) where

import Testing
import Language.CP.Run (runProgram)

interpreterTests :: TestTree
interpreterTests = testGroup "InterpreterTests" $
  map (\(n,(r,t)) -> testCase ("InterpreterTest " ++ show n) (assertEq . (,t) =<< r)) $ zip [0..] tests


type InterpreterTest =
  (IO (Either Error Value), Either Error Value)

tests :: [InterpreterTest]
tests =
  [(runProgram "test" $ unlines
      ["fun main[] -> Int {"
      ,"  f();"
      ,"};"
      ,"fun f[] -> Int { 5; };"
      ]
   , Right $ VInt 5
   )
  ,(runProgram "test" $ unlines
      ["fun main[] -> Int {"
      ,"  if(true, 6, 5);"
      ,"};"
      ]
   , Right $ VInt 6
   )
  ,(runProgram "test" $ unlines
      ["fun main[] -> Int {"
      ,"  if(false, 6, 5);"
      ,"};"
      ]
   , Right $ VInt 5
   )
  ,(runProgram "test" $ unlines
      ["fun main[] -> Int {"
      ,"  f(5);"
      ,"};"
      ,""
      ,"fun f[x : Int] -> Int {"
      ,"  x;"
      ,"};"
      ]
   , Right $ VInt 5
   )
  ,(runProgram "test" $ unlines
      ["fun main[] -> Int {"
      ,"  val x : Int = 5;"
      ,"  {"
      ,"    val x : Int = 6;"
      ,"    f(x);"
      ,"  }"
      ,"  f(x);"
      ,"};"
      ,""
      ,"fun f[x : Int] -> Int {"
      ,"  x;"
      ,"};"
      ]
    , Right $ VInt 5
    )

  ,(runProgram "test" $ unlines
      [ "fun main[] -> Int {"
      , "  doWith2(const,4);"
      , "};"
      , "fun const[x : Int, y : Int] -> Int {"
      , "  x;"
      , "};"
      , "fun doWith2[do : [Int, Int] -> Int, y : Int] -> Int {"
      , "  do(y, 2);"
      , "};"
      ]  
    , Right $ VInt 4
    )

  ,(runProgram "test" $ unlines
      [ "fun main[] -> Void {"
      , "  printInt(doWith2(add,2));"
      , "};"
      , "fun doWith2[do : [Int, Int] -> Int, y : Int] -> Int {"
      , "  do(y, 2);"
      , "};"
      ]  
   , Right VVoid
   )
    
  ,(runProgram "test" $ unlines
      [ "fun main[] -> Int {"
      , "  doWith2(add,2);"
      , "};"
      , "fun doWith2[do : [Int, Int] -> Int, y : Int] -> Int {"
      , "  do(y, 2);"
      , "};"
      ]  
   , Right $ VInt 4
   )
  ]
