module Testing
  ( module Testing
  ) where

import Test.Tasty as Testing
import Test.Tasty.HUnit as Testing

import Language.CP.Syntax as Testing
import Language.CP.Parser as Testing
import Language.CP.Errors as Testing

assertEq :: (Eq a, Show a) => (a, a) -> Assertion
assertEq (x,y) = y @=? x
