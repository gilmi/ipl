fun main[] -> Void {
  printInt(doWith2(add,2));
};
fun doWith2[do : [Int, Int] -> Int, y : Int] -> Int {
  do(y, 2);
};
 

