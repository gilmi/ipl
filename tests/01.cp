fun main[] -> Int {
  val x : Int = 5;
  {
    val x : Int = 6;
    f(x);
  }
  f(x);
};

fun f[x : Int] -> Int {
  x;
};
