module Language.CP.Syntax where

import qualified Data.Text as T


type Program = [Decl]

data Decl
  = DFun Name [(Name, Type)] Type [Statement]
  | DVal Name Type Expr
  | DType Name Type
  deriving (Eq, Ord, Show)

declName :: Decl -> String
declName = \case
  DFun n _ _ _ -> T.unpack n
  DVal n _ _ -> T.unpack n
  DType n _ -> T.unpack n

declType :: Decl -> Either Type Type
declType = \case
  DFun _ at rt _ -> Right $ TFun (map snd at) rt
  DVal _ t _ -> Right t
  DType _ t -> Left t

data Type
  = TCon String
  | TVar String
  | TStruct [(Name, Type)]
  | TFun [Type] Type
  deriving (Eq, Ord, Show)

builtin :: [Type]
tvoid, tbool, tchar, tint, tstring :: Type
builtin@[tvoid, tbool, tchar, tint, tstring] = map TCon
  [ "Void"
  , "Bool"
  , "Char"
  , "Int"
  , "String"
  ]

vvoid :: Expr
vvoid = Value VVoid

vbool :: Bool -> Expr
vbool = Value . VBool

vchar :: Char -> Expr
vchar = Value . VChar

vint :: Integer -> Expr
vint = Value . VInt

vstring :: T.Text -> Expr
vstring = Value . VString

type Name = T.Text

data Statement
  = SExpr Expr
  | SDecl Decl
  | SBlock [Statement]
  deriving (Show, Eq, Ord)

data Expr
  = Value Value
  | App Name [Expr]
  | Var Name
  deriving (Show, Eq, Ord)

data Value
  = VVoid
  | VBool Bool
  | VChar Char
  | VInt Integer
  | VString T.Text
  | VStruct [(Name, Value)]
  | VFun Name
  deriving (Eq, Ord, Show)
