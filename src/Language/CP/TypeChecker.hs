{-# LANGUAGE OverloadedStrings #-}

module Language.CP.TypeChecker where

import Prelude hiding (lookup)
import Control.Monad
import Control.Arrow ((&&&))
import Data.Bifunctor
import Data.Either
import Data.Maybe (catMaybes)
import Data.Monoid ((<>))

import qualified Data.List as L (lookup)
import qualified Data.Text as T
import qualified Control.Monad.Trans.State as MT
import qualified Control.Monad.Trans.Except as MT
import qualified Control.Monad.Except as MTL
import qualified Control.Monad.State as MTL

import Language.CP.Syntax

type TypeCheckM =
  MT.ExceptT TypeErr (MT.State State)


data State = State
  { sEnv  :: [ScopedEnv]
  , sTEnv :: [ScopedTEnv]
  , sGlobalEnv  :: [(Name, Type)]
  , sGlobalTEnv :: [(Type, Type)]
  , sGen  :: Int
  }
  deriving (Show, Eq)

initState :: State
initState = State [[]] [[]] builtinEnv [] 0

builtinEnv :: [(Name, Type)]
builtinEnv = concat
  [ [ (n, TFun [tint, tint] tint) | n <- ["add", "sub", "mul", "div"] ]
  , [ ("print" <> n, TFun [t] tvoid)
    | (n,t) <- [("Int", tint), ("Bool", tbool), ("Void", tvoid), ("String", tstring), ("Char", tchar)]
    ]
  ]

data Scope a
  = EVar (a,Type)
  | NewScope
  deriving (Show, Eq)

scopeToMaybe :: Scope a -> Maybe (a,Type)
scopeToMaybe = \case
  EVar x -> Just x
  NewScope -> Nothing

{- | Environment needs:

  - Global declarations to be global
  - Block scoped environment
  - Closure environment

-}
type Env  = [(Name, Type)]
type TEnv = [(Type, Type)]

type ScopedEnv  = [Scope Name]
type ScopedTEnv = [Scope Type]

data TypeErr
  = TypeMismatch (Maybe Expr) Type Type
  | TypeUnifyFail Type Type
  | NotInEnv  Name Env
  | NotInTEnv Type TEnv
  | ArityMismatch Expr Int Int
  | NotAFunction Expr Type
  | DeclAtEnd
  | UnexpectedDecl Decl
  | EmptyBlock
  deriving (Show, Eq)

checkProgram :: Program -> Either TypeErr ()
checkProgram program =
  let (types,decls) = partitionEithers $ map (\d -> bimap (declName d,) (declName d,) $ declType d) program
      is = initState
             { sGlobalEnv = map (first T.pack) decls ++ sGlobalEnv initState
             , sGlobalTEnv = map (first TCon) types ++ sGlobalTEnv initState
             }
  in do
    MTL.evalState (MTL.runExceptT $ mapM_ inferDecl $ snd $ partitionDecls program) is

partitionDecls :: Program -> ([(Name, Type)], [Decl])
partitionDecls = \case
  [] ->
    ([],[])
  DType n t : rest ->
    case partitionDecls rest of
    (ts, ds) -> ((n,t) : ts, ds)
  d : rest ->
    case partitionDecls rest of
    (ts, ds) -> (ts, d : ds)

inferV :: Value -> Type
inferV = \case
  VVoid ->
    TCon "Void"
  VBool _ ->
    TCon "Bool"
  VChar _ ->
    TCon "Char"
  VInt _ ->
    TCon "Int"
  VString _ ->
    TCon "String"
  VStruct list ->
    TStruct (fmap (fmap inferV) list)

runInferE :: Expr -> State -> (Either TypeErr Type, State)
runInferE e s =
  MTL.runState (MTL.runExceptT $ inferE e) s

-- | infer an expression
inferE :: Expr -> TypeCheckM Type
inferE = \case
  Value v -> pure (inferV v)
  Var n ->
    lookupM n
  e@(App "if" [test,true,false]) -> do
    equals e tbool =<< inferE test
    f <- inferE true
    t <- inferE false
    equals e f t
    pure t

  e@(App n es) ->
    lookupM n >>= \case
      TFun ts t -> do
        MTL.when
            (length ts /= length es)
            (MTL.throwError $ ArityMismatch e (length ts) (length es))
        MTL.zipWithM_ (equals e) ts =<< traverse inferE es
        pure t

      t ->
        MTL.throwError (NotAFunction e t)


equals :: Expr -> Type -> Type -> TypeCheckM ()
equals e t1 t2 = case (t1,t2) of
  (TVar tn1, TVar tn2) | tn1 == tn2 -> pure ()
  (TVar _, _) -> extendTM t1 t2
  (_, TVar _) -> extendTM t2 t1
  _ | t1 == t2 -> pure ()
  _ -> MTL.throwError (TypeMismatch (pure e) t1 t2)

newEnvM :: TypeCheckM ()
newEnvM = do
  state <- MTL.get
  MTL.put state { sEnv = [] : sEnv state, sTEnv = [] : sTEnv state }

withScope :: TypeCheckM a -> TypeCheckM a
withScope t = scope *> t <* unscope

scope :: TypeCheckM ()
scope = do
  state <- MTL.get
  let (es:env, ts:tenv) = (sEnv &&& sTEnv) state
  MTL.put state { sEnv = (NewScope : es) : env, sTEnv = (NewScope : ts) : tenv }

unscope :: TypeCheckM ()
unscope = do
  state <- MTL.get
  let (es:env, ts:tenv) = (sEnv &&& sTEnv) state
  let es' = (dropWhile (==NewScope) . dropWhile (/=NewScope)) es
  let ts' = (dropWhile (==NewScope) . dropWhile (/=NewScope)) ts
  MTL.put state { sEnv = es' : env, sTEnv = ts' : tenv }

getEnv :: State -> Env
getEnv state = (catMaybes . map scopeToMaybe . head . sEnv) state
            ++ sGlobalEnv state

getTEnv :: State -> TEnv
getTEnv state = (catMaybes . map scopeToMaybe . head . sTEnv) state
             ++ sGlobalTEnv state

checkExprM :: Type -> Expr -> TypeCheckM ()
checkExprM type' expr = do
  inferred <- inferE expr
  when (type' /= inferred) $
    MTL.throwError (TypeMismatch (pure expr) type' inferred)


inferDecl :: Decl -> TypeCheckM Type
inferDecl = \case
  DVal name mType expr -> do
    extendM name mType
    withScope $ do
      checkExprM mType expr
      pure mType

  DType name type' -> do
    extendTM (TVar $ T.unpack name) type'
    pure type'

  DFun name args retType stmts -> do
    extendM name (TFun (map snd args) retType)
    withScope $ do
      MTL.forM_ args (uncurry extendM)
      checkStmtsM retType stmts
      pure (TFun (map snd args) retType)

checkStmtsM :: Type -> [Statement] -> TypeCheckM ()
checkStmtsM t = \case
  [] -> MTL.throwError $ EmptyBlock
-- [] | t /= tvoid -> MTL.throwError $ TypeMismatch Nothing t tvoid
-- [] -> pure ()
  [stmt] -> case stmt of
    SDecl{} ->
      MTL.throwError DeclAtEnd
    SBlock stmts' -> do
      scope
      checkStmtsM t stmts'
      <* unscope
    SExpr e ->
      checkExprM t e

  stmt:rest -> case stmt of
    SDecl d@(DVal{}) -> do
      void $ inferDecl d
      checkStmtsM t rest
    SDecl d -> do
      MTL.throwError (UnexpectedDecl d)
    SBlock stmts' -> do
      scope
      checkStmtsM t stmts'
      <* unscope
    SExpr e -> do
      void $ inferE e
      checkStmtsM t rest



lookupT :: Type -> TypeCheckM (Maybe Type)
lookupT t' = do
  (`MTL.catchError` const (pure Nothing)) $
    fmap pure $ lookupTM t' >>= \case
      t@(TCon n) ->
        if t `elem` builtin
        then pure t
        else lookupM (T.pack n)
      t -> pure t

lookupTM :: Type -> TypeCheckM Type
lookupTM t' = do
  env <- getTEnv <$> MTL.get
  case L.lookup t' env of
    Nothing ->
      MTL.throwError (NotInTEnv t' env)
    Just t ->
      pure t

lookup :: Name -> TypeCheckM (Maybe Type)
lookup name = do
  (`MTL.catchError` const (pure Nothing)) $
    fmap pure $ lookupM name >>= \case
      t@(TCon _) ->
        if t `elem` builtin
        then pure t
        else lookupTM t
      t -> pure t

lookupM :: Name -> TypeCheckM Type
lookupM name = do
  env <- getEnv <$> MTL.get
  case L.lookup name env of
    Nothing ->
      MTL.throwError (NotInEnv name env)
    Just t ->
      pure t

extendTM :: Type -> Type -> TypeCheckM ()
extendTM t1 t2 = do
  state <- MTL.get
  let (ts:tenv) = sTEnv state
  MTL.put state { sTEnv = (EVar (t1,t2) : ts) : tenv }

extendM :: Name -> Type -> TypeCheckM ()
extendM name type' = do
  state <- MTL.get
  let (es:env) = sEnv state
  MTL.put state { sEnv = (EVar (name,type') : es) : env }

gen :: TypeCheckM Type
gen = do
  state <- MTL.get
  MTL.put state { sGen = sGen state + 1 }
  pure (TVar $ "t" ++ show (sGen state))
