{-# LANGUAGE OverloadedStrings #-}

module Language.CP.Parser where

import Control.Applicative ((<|>))

import Data.Bifunctor (first)
import qualified Data.Text as T

import qualified Text.Megaparsec as Prs
import qualified Text.Megaparsec.Text as Prs


import           Language.CP.Syntax
import           Language.CP.Lexer


parse :: String -> String -> Either String Program
parse name prog =
  first Prs.parseErrorPretty $ Prs.parse program name (T.pack prog)

program :: Prs.Parser Program
program = Prs.many (decl <* semicolon) <* Prs.eof

decl :: Prs.Parser Decl
decl = funDef <|> valDef <|> typeDef

funDef :: Prs.Parser Decl
funDef = rword "fun" *>
  (DFun <$> identifier
        <*> funArgs
        <*> (rword "->" *> type_)
        <*> braces statements)

funArgs :: Prs.Parser [(Name, Type)]
funArgs =
  brackets $ Prs.sepBy
    ((,) <$> identifier
         <*> (colon *> type_))
    comma

valDef :: Prs.Parser Decl
valDef = rword "val" *>
  (DVal <$> identifier
        <*> (colon *> type_)
        <*> (equals *> expr))

typeDef :: Prs.Parser Decl
typeDef = rword "type" *>
  (DType <$> typer
         <*> (equals *> type_))

type_ :: Prs.Parser Type
type_ =
      (TCon <$> fmap T.unpack typer)
  <|> Prs.try (fmap TStruct structType)
  <|> (TFun <$> (brackets $ Prs.sepBy type_ comma) <*> (rword "->" *> type_))


structType :: Prs.Parser [(Name, Type)]
structType =
  brackets $ Prs.sepBy
    ((,) <$> identifier
         <*> (colon *> type_))
    comma

statements :: Prs.Parser [Statement]
statements = Prs.many statement

statement :: Prs.Parser Statement
statement =
      ((SDecl <$> decl) <* semicolon)
  <|> ((SExpr <$> expr) <* semicolon)
  <|> (SBlock <$> braces statements)


expr :: Prs.Parser Expr
expr =
    (Value <$> value)
  <|> Prs.try (App <$> identifier <*> parens (Prs.sepBy expr comma))
  <|> (Var <$> identifier)

value :: Prs.Parser Value
value =
    ((const (VBool True) <$> rword "true") <|> (const (VBool False) <$> rword "false"))
  <|> (VChar   <$> char)
  <|> (VInt    <$> integer)
  <|> (VString <$> string)
  <|> (VStruct <$> brackets (Prs.sepBy ((,) <$> identifier <*> (equals *> value)) comma))
