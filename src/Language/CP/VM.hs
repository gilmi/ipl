{-# LANGUAGE OverloadedStrings #-}

module Language.CP.VM
  (runFromFile
  ,runProgram
  ,parse
  ,initEnv
  ,run
  )
where

import qualified Data.ByteString as BS
import qualified Data.Vector as V
import Data.Bool
import Data.Word
import Prelude hiding (lookup)

import qualified Control.Monad.Trans.Except as M
import qualified Control.Monad.Trans.State as M
import qualified Control.Monad.Identity as M
import Control.Monad.State
import Control.Monad.Except

runFromFile :: FilePath -> IO ()
runFromFile file = do
  content <- BS.readFile file
  print $ runProgram content


runProgram :: BS.ByteString -> Either Error ()
runProgram bs = do
  instructions <- parse bs
  M.runIdentity $ M.evalStateT (M.runExceptT  run) (initEnv instructions)

parse :: MonadError Error m => BS.ByteString -> m [Instruction]
parse = go . BS.unpack
  where
    go = \case
      [] -> pure []
      0x00:rest -> (Halt:) <$> go rest
      0x60:rest -> (IAdd:) <$> go rest
      0x68:rest -> (IMul:) <$> go rest
      0x10:v:rest -> (Bipush v:) <$> go rest
      0x15:v:rest -> (ILoad v:)  <$> go rest
      0x36:v:rest -> (IStore v:) <$> go rest
      0x99:v:rest -> (Ifeq v:) <$> go rest
      0xa7:v:rest -> (Goto v:) <$> go rest
      x:_ -> throwError $ ParseError x


data Instruction
  = Bipush Word8
  | ILoad Word8
  | IStore Word8
  | Goto Word8
  | Ifeq Word8
  | IAdd
  | IMul
  | Halt
  deriving (Show, Read, Eq, Ord)

data Env = Env
  { stack :: V.Vector Word8
  , heap  :: V.Vector HeapValue
  , program :: V.Vector Instruction
  , sp :: Int
  , hp :: Int
  , pc :: Int
  }
  deriving (Show, Read, Eq, Ord)


initEnv :: [Instruction] -> Env
initEnv p = Env
  { stack = V.replicate 0xFF 0
  , heap  = V.replicate 0xFF (Data 0)
  , program = V.fromList p
  , sp = 0
  , hp = 0
  , pc = 0
  }

data HeapValue
  = Begin Int -- ^ Beginning of data block of size Int
  | Data Word8 -- ^ data
  deriving (Show, Read, Eq, Ord)

fromBegin :: MonadError Error m => Word8 -> HeapValue -> m Int
fromBegin i = \case
  Begin s -> pure s
  Data _ -> throwError $ UnexpectedHeapAddress i

fromData :: MonadError Error m => Word8 -> HeapValue -> m Word8
fromData i = \case
  Begin _ -> throwError $ UnexpectedHeapAddress i
  Data d -> pure d
  

data Error
  = Err String
  | StackOverflow (V.Vector Word8)
  | ProgramOverflow
  | UnexpectedHeapAddress Word8
  | ParseError Word8
  deriving (Show, Read, Eq, Ord)

type VM = M.ExceptT Error (M.State Env)

runGC :: VM ()
runGC = undefined

run :: VM ()
run = do
  Env{..} <- get
  when (hp+1 >= V.length heap)
    runGC
  when (pc >= V.length program)
    (throwError ProgramOverflow)
  runInst (program V.! sp) >>= bool (pure ()) run

runInst :: Instruction -> VM Bool
runInst inst = do
  env@Env{..} <- get
  case inst of
    Halt ->
      pure False

    Bipush v -> do
      let
        stack' = stack V.// [(sp, v)]
        sp' = sp + 1
        pc' = pc + 1
      put env { pc = pc', sp = sp', stack = stack' }
      pure True
      
    IAdd -> do
      op "iadd" 2 1 (V.singleton . V.sum)
      pure True
      
    IMul -> do
      op "imul" 2 1 (V.singleton . V.product)
      pure True

    ILoad v -> do
      size <- fromBegin v (heap V.! fromIntegral v)
      block <-
        mapM
          (uncurry fromData)
          (V.zip
             (V.fromList [(v + 1)..(fromIntegral size)])
             (V.slice (fromIntegral v + 1) size heap))
      op "iload" 0 (V.length block) (const block)
      pure True

    IStore v -> do
      let block = V.fromList [Begin 1, Data v]
          heap' = heap `V.update` V.zip (V.fromList [hp..(hp + 2)]) block
      put env { heap = heap' }
      pure True

    Goto v -> do
      put env { pc = fromIntegral v }
      pure True

    Ifeq v -> do
      unless (sp >= 1)
        (throwError $ Err "Not enough arguments to ifeq. expected 1 but got: 0")
      if (stack V.! sp - 1) == 0
        then runInst (Goto v)
        else do
          put env { pc = pc + 1 }
          pure True



op :: String -> Int -> Int -> (V.Vector Word8 -> V.Vector Word8) -> VM ()
op s n m f = do
  env@Env{..} <- get
  unless (sp >= n)
    (throwError $ Err $ "Not enough arguments to " ++ s ++ ". expected " ++ show n ++ " but got: " ++ show sp)
  unless (sp - n + m < V.length stack)
    (throwError $ StackOverflow stack)
  let
    args = V.slice (sp - n) n stack
    results = f args
    stack' = stack `V.update` V.zip (V.fromList [(sp - n)..(sp - n + m)]) results
    sp' = sp - (V.length results)
    pc' = pc + 1
  put env { pc = pc', sp = sp', stack = stack' }
