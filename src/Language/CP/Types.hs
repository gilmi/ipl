module Language.CP.Types where

import qualified Data.Text as T

data ErrMsg e
  = Error e T.Text
    deriving (Eq, Ord, Show)

data Location = Location
  { locName   :: T.Text
  , locInput  :: T.Text
  , locCursor :: T.Text
  , locLine   :: Int
  , locColumn :: Int
  } deriving (Eq, Ord, Show)

mkLocation :: T.Text -> T.Text -> Location
mkLocation name txt =
  Location name txt txt 0 0

data WithLoc a
  = WithLoc Location a
    deriving (Eq, Ord, Show)

instance Functor WithLoc where
  fmap f (WithLoc l x) = WithLoc l (f x)
