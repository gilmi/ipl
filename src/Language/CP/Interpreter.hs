{-# LANGUAGE OverloadedStrings #-}

module Language.CP.Interpreter where

import Prelude hiding (lookup)
import Control.Applicative ((<|>))
import Data.Maybe (mapMaybe)
import Data.Monoid ((<>))

import qualified Data.List as L (lookup, find, intercalate)
import qualified Data.Text as T
import qualified Control.Monad.Trans.State as MT
import qualified Control.Monad.Trans.Except as MT
import qualified Control.Monad.Except as MTL
import qualified Control.Monad.State as MTL

import Language.CP.Syntax


type InterpretM =
  MT.ExceptT InterpretErr (MT.StateT State IO)

data State = State
  { sEnv  :: [ScopedEnv]
  , sGlobalEnv :: [Decl]
  }
  deriving (Show, Eq)

data Scope
  = VVar (Name, Value)
  | NewScope
  deriving (Show, Eq)

scopeToMaybe :: Scope -> Maybe (Name, Value)
scopeToMaybe = \case
  VVar x -> Just x
  NewScope -> Nothing

{- | Environment needs:

  - Global declarations to be global
  - Block scoped environment
  - Closure environment

-}
type Env  = [(Name, Decl)]

type ScopedEnv = [Scope]

data InterpretErr
  = InternalInterpretError String
  | NoMain
  | NotInEnv Name State
  | VFunLookupFailed String
  deriving (Show, Eq)

interpret :: Program -> IO (Either InterpretErr Value)
interpret p =
  MTL.evalStateT (MTL.runExceptT $ program p) (initState p)

initState :: Program -> State
initState = State [[]]

program :: Program -> InterpretM Value
program p = case L.find ((== "main") . declName) p of
  Just (DFun _ _ _ body) ->
    head . reverse <$> traverse statement body
  _ -> MTL.throwError NoMain

getScopedEnv :: [ScopedEnv] -> [(Name, Value)]
getScopedEnv = mapMaybe scopeToMaybe . concat . take 1

statements :: [Statement] -> InterpretM Value
statements ss = head . reverse <$> traverse statement ss

statement :: Statement -> InterpretM Value
statement = \case
  SExpr e -> expression e
  SDecl (DVal n _ e) -> do
    res <- expression e
    addToEnv (VVar (n, res))
    pure VVoid
  SDecl d ->
    MTL.throwError $ InternalInterpretError $ "Unexpected declaration. Should have been caught with typechecking: " ++ show d
  SBlock ss -> do
    addToEnv NewScope
    result <- head . reverse <$> traverse statement ss
    dropScope
    pure result


expression :: Expr -> InterpretM Value
expression = \case
  Value v ->
    pure v

  Var n -> do
    lookupM n `MT.catchE` \err ->
      case L.lookup n builtins of
        Just _ -> pure $ VFun n
        Nothing -> MT.throwE err

  App n es -> case L.lookup n builtins of
    Just f -> f es
    Nothing -> do
      funLookupM n >>= \case
        Right (names, body) -> do
          args <- traverse expression es
          withScope (zip names args) body
        Left fn -> case L.lookup fn builtins of
          Just f -> f es
          Nothing -> MTL.throwError $ VFunLookupFailed $ T.unpack fn

withScope :: [(Name, Value)] -> [Statement] -> InterpretM Value
withScope newEnv p = do
  MTL.modify $ \env -> env { sEnv = map VVar newEnv : sEnv env }
  result <- statements p
  MTL.modify $ \env -> env { sEnv = tail $ sEnv env }
  pure result

addToEnv :: Scope -> InterpretM ()
addToEnv s =
  MTL.modify $ \env -> case sEnv env of { h:t -> env { sEnv = (s : h) : t } }

dropScope :: InterpretM ()
dropScope = do
  env <- MTL.get
  case sEnv env of
    e:es -> MTL.put env { sEnv = drop 1 (dropWhile (/= NewScope) e) : es }
    [] -> MTL.throwError (InternalInterpretError "Trying to drop NewScope but nothing in env")

lookupM :: Name -> InterpretM Value
lookupM n = do
  env  <- getScopedEnv . sEnv <$> MTL.get
  genv <- sGlobalEnv <$> MTL.get
  case fmap Value (L.lookup n env) <|> glookup genv of
    Just x ->
      expression x

    Nothing -> do
      MTL.throwError . NotInEnv n =<< MTL.get

  where
    glookup genv = L.find ((==n) . T.pack . declName) genv >>= \case
      DVal _ _ e -> pure e
      DFun n' _ _ _ -> pure (Value $ VFun n')
      _ -> const (Value $ VFun n) <$> L.lookup n builtins

funLookupM :: Name -> InterpretM (Either Name ([Name], [Statement]))
funLookupM n' = do
  env <- MTL.get
  let
    go n = case L.find ((==n) . T.pack . declName) $ sGlobalEnv env of
      Just (DFun _ es _ ss) -> pure (map fst es, ss)
      _ -> MTL.throwError . NotInEnv n =<< MTL.get

  MT.catchE
    (do lookupM n' >>= \case
         VFun name -> fmap Right (go name) `MT.catchE` const (pure $ Left name)
         _ -> fmap Right (go n')
    ) $ const $ fmap Right (go n')

builtins :: [(Name, [Expr] -> InterpretM Value)]
builtins =
  [ binIntOp "add" (+)
  , binIntOp "sub" (-)
  , binIntOp "mul" (*)
  , binIntOp "div" div
  , ("if", ifExpr)
  ]
  ++ [ ("print" <> t, printExpr) | t <- ["Int", "Bool", "Void", "String", "Char"] ]

binIntOp :: Name -> (Integer -> Integer -> Integer) -> (Name, [Expr] -> InterpretM Value)
binIntOp name f = (name,) $ \es -> do
  traverse expression es >>= \case
    [VInt x,VInt y] -> pure $ VInt $ f x y
    xs -> MTL.throwError $ InternalInterpretError $
      "Invalid arguments to " <> T.unpack name <> ": " <> show xs
  

ifExpr :: [Expr] -> InterpretM Value
ifExpr = \case
  [test, true, false] -> do
    expression test >>= \case
      VBool True  -> expression true
      VBool False -> expression false
      v -> MTL.throwError $ InternalInterpretError $
        "Unexpected result for if " <> show v
  xs -> MTL.throwError $ InternalInterpretError $
    "Invalid arguments to if " <> show xs
  
printExpr :: [Expr] -> InterpretM Value
printExpr es = do
  vals <- traverse expression es
  MTL.liftIO $ putStrLn $ L.intercalate ", " $ map printVal vals
  pure VVoid

printVal :: Value -> String
printVal = \case
  VVoid -> "void"
  VBool b -> if b then "true" else "false"
  VChar c -> show c
  VInt i -> show i
  VString s -> show s
  VStruct s -> "[ " ++ L.intercalate ", " (map (\(n,v) -> T.unpack n ++ " = " ++ printVal v) s) ++ "]" 
  VFun n -> "<function:" ++ T.unpack n ++ ">"
