{-# LANGUAGE OverloadedStrings #-}

module Language.CP.Lexer where

import Control.Monad (void)
import Control.Applicative ((<|>))

import qualified Data.Text as T

import qualified Text.Megaparsec as Prs
import qualified Text.Megaparsec.Text as Prs
import qualified Text.Megaparsec.Lexer as Lex


-- | Defining what is considered a space to consume
spaceConsumer :: Prs.Parser ()
spaceConsumer = Lex.space (void Prs.spaceChar) lineCmnt blockCmnt
  where
    lineCmnt  = Lex.skipLineComment "//"
    blockCmnt = Lex.skipBlockComment "/*" "*/"

lexeme :: Prs.Parser a -> Prs.Parser a
lexeme = Lex.lexeme spaceConsumer

symbol :: T.Text -> Prs.Parser T.Text
symbol = fmap T.pack . Lex.symbol spaceConsumer . T.unpack

-- | 'integer' parses an integer
integer :: Prs.Parser Integer
integer = lexeme (Lex.signed spaceConsumer Lex.integer)

-- | strings
string :: Prs.Parser T.Text
string = fmap T.pack (Prs.char '"' >> Prs.manyTill Lex.charLiteral (Prs.char '"'))

-- | char
char :: Prs.Parser Char
char = Prs.char '\'' *> Lex.charLiteral <* Prs.char '\''

-- | 'semi' parses a semicolon
semi :: Prs.Parser T.Text
semi = symbol ";"

rword :: T.Text -> Prs.Parser ()
rword w = Prs.string (T.unpack w) *> Prs.notFollowedBy Prs.alphaNumChar *> spaceConsumer

-- | list of reserved words
reservedWords :: [T.Text]
reservedWords = ["while","skip","true","false", "fun", "val", "type", "->"]

-- | identifiers
identifier :: Prs.Parser T.Text
identifier = lexeme (p >>= check . T.pack)
  where
    p       = (:) <$> idCharStart <*> Prs.many idCharRest
    check x = if x `elem` reservedWords
                then fail $ "keyword " ++ show x ++ " cannot be an identifier"
                else pure x
    idCharStart = Prs.letterChar   <|> Prs.oneOf ("!@#$%^&*-=/_" :: String)
    idCharRest  = Prs.alphaNumChar <|> Prs.oneOf ("!@#$%^&*-=/_" :: String)

-- | types - alphanums with first upper char
typer :: Prs.Parser T.Text
typer = lexeme (p >>= check . T.pack)
  where
    p       = (:) <$> tyCharStart <*> Prs.many tyCharRest
    check x = if x `elem` reservedWords
                then fail $ "keyword " ++ show x ++ " cannot be an identifier"
                else pure x
    tyCharStart = Prs.upperChar
    tyCharRest  = Prs.alphaNumChar

-- | polymorphic types - alphanums with first upper char
pTyper :: Prs.Parser T.Text
pTyper = lexeme (p >>= check . T.pack)
  where
    p       = (:) <$> tyCharStart <*> Prs.many tyCharRest
    check x = if x `elem` reservedWords
                then fail $ "keyword " ++ show x ++ " cannot be an identifier"
                else pure x
    tyCharStart = Prs.lowerChar
    tyCharRest  = Prs.alphaNumChar



-- | 'parens' parses something between parenthesis
parens :: Prs.Parser a -> Prs.Parser a
parens = Prs.between (symbol "(") (symbol ")")

braces, angles, brackets :: Prs.Parser a -> Prs.Parser a
braces    = Prs.between (symbol "{") (symbol "}")
angles    = Prs.between (symbol "<") (symbol ">")
brackets  = Prs.between (symbol "[") (symbol "]")

semicolon, comma, colon, dot, equals :: Prs.Parser T.Text
semicolon = symbol ";"
comma     = symbol ","
colon     = symbol ":"
dot       = symbol "."
equals    = symbol "="
