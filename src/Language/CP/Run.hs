{-# LANGUAGE OverloadedStrings #-}

module Language.CP.Run where

import Control.Monad
import Data.Bifunctor
import Data.Monoid
import Text.Groom (groom)
import System.Exit (exitSuccess)
import System.IO (hSetBuffering, stdout, BufferMode(NoBuffering))

import           Language.CP.Syntax
import qualified Language.CP.Parser as P
import qualified Language.CP.TypeChecker as T
import qualified Language.CP.Interpreter as I

import Language.CP.Errors

repl :: IO ()
repl = do
  hSetBuffering stdout NoBuffering
  putStrLn "cpi version 0.1, type :q to quit"
  forever $ do
    putStr "> "
    getLine >>= \case
        ':':cmd -> runCommand cmd
        expr -> runExpr expr


runExpr :: String -> IO ()
runExpr s = do
  let e = ((\p -> bimap TypeErr (const p) $ T.checkProgram p) <=< mapErr ParseErr . P.parse "REPL") s
  case e of
    Left err -> putStrLn $ printError err
    Right r -> putStrLn . either printError groom . mapErr InterpretErr =<< I.interpret r


runProgram :: String -> String -> IO (Either Error Value)
runProgram file code = do
  let e = ((\p -> bimap TypeErr (const p) $ T.checkProgram p) <=< mapErr ParseErr . P.parse file) code
  either (pure . Left) (fmap (mapErr InterpretErr) . I.interpret) e

runProgramFromFile :: [String] -> IO ()
runProgramFromFile = \case
  file:[] -> do
    code <- readFile file
    putStrLn . either printError groom =<< runProgram file code
  _ -> putStrLn "This command runs a program and needs one file name argument"

runCommand :: String -> IO ()
runCommand x = case words x of
  [] -> putStrLn "Empty command"
  cmd:args ->
    maybe
      (putStrLn $ "Unknown command: " <> cmd)
      ($ args)
      (lookup cmd commands)

commands :: [(String, [String] -> IO ())]
commands =
  [("q", const quit)
  ,("quit", const quit)
  ,("p", runProgramFromFile)
  ,("program", runProgramFromFile)
  ]


quit :: IO ()
quit = do
  putStrLn "Leaving cpi."
  exitSuccess
