module Language.CP.Errors where

import Text.Groom (groom)
import qualified Language.CP.TypeChecker as T
import qualified Language.CP.Interpreter as I

data Error
  = ParseErr String
  | TypeErr T.TypeErr
  | InterpretErr I.InterpretErr
  deriving (Eq, Show)

mapErr :: (a -> b) -> Either a c -> Either b c
mapErr f = either (Left . f) Right

printError :: Error -> String
printError = \case
  ParseErr s -> s
  x -> groom x
