{-# LANGUAGE OverloadedStrings #-}

module Language.CP.Checks where

import Data.List
import Data.Ord (comparing)
import Data.Function (on)
import qualified Control.Monad.Except as MTL

import Language.CP.Syntax
import Language.CP.TypeChecker

data Error
  = DupDecls [[Decl]]
  | TypeError TypeErr
  | Error Error
  deriving Show

checkMultipleDecsM :: MTL.MonadError Error m => Program -> m ()
checkMultipleDecsM prog =
  let l = filter ((>1) . length) . groupBy ((==) `on` declName) . sortBy (comparing declName) $ prog
  in if null l
     then pure ()
     else MTL.throwError (DupDecls l)
